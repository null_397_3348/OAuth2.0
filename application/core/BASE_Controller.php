<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class BASE_Controller extends CI_Controller {

	protected $_user_id = 0; //当前用户id
	protected $_openid = 0; //openid
	protected $_app_key = ''; //appkey
	protected $_app_id = 0; //appid
	protected $_access_token = '';//授权token
	protected $_scope = '';//授权token

	protected $_errno = '';

	public function __construct() {
		parent::__construct();
		$this->_access_token = $this->input->get_post('access_token', TRUE);
		$this->_openid = $this->input->get_post('openid', TRUE);

		$this->init_publicload();
		$this->checkAccessToken();
	}

	public function init_publicload() {
		$this->load->config('errno');
		$this->load->helper('output');
		$this->load->model(array('oauth2/OAuth'));
		$this->_errno = $this->config->item('errno');
	}

	public function checkAccessToken() {
		$ret = $this->OAuth->getAccessToken($this->_access_token, $this->_openid);
		if (false !== $ret && !empty($ret['user_id'])) {
			$this->_user_id = $ret['user_id'];
			$this->_scope = $ret['scope'];
			$this->openid = $ret['openid'];
		} else {
			error_output(ERROR_INVALID_ACCESS_TOKEN, $this->_errno);
		}
	}

}
