-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.10-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 oauth2 的数据库结构
CREATE DATABASE IF NOT EXISTS `oauth2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `oauth2`;


-- 导出  表 oauth2.app 结构
CREATE TABLE IF NOT EXISTS `app` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(128) NOT NULL COMMENT '应用名称',
  `app_key` varchar(128) NOT NULL COMMENT '应用标识',
  `app_secret` varchar(128) NOT NULL COMMENT '应用secret',
  `redirect_uri` varchar(1024) NOT NULL COMMENT '授权回调地址',
  `type_id` bigint(25) NOT NULL COMMENT '应用分类ID',
  `channel_id` bigint(25) NOT NULL,
  `developer_id` bigint(25) NOT NULL COMMENT '开发者ID',
  `om_id` bigint(25) NOT NULL COMMENT '公众账号ID',
  `summary` varchar(1024) NOT NULL COMMENT '应用介绍',
  `logo_url` varchar(1024) NOT NULL COMMENT 'logo_url',
  `pic_urls` text NOT NULL,
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_key` (`app_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 oauth2.oauth2_codes 结构
CREATE TABLE IF NOT EXISTS `oauth2_codes` (
  `code` varchar(40) NOT NULL COMMENT '置换token中间码',
  `app_key` varchar(128) NOT NULL COMMENT '应用标识',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `redirect_uri` varchar(1024) NOT NULL COMMENT '认证回调地址',
  `expires` int(11) NOT NULL COMMENT 'code有效期',
  `scope` varchar(200) DEFAULT NULL COMMENT '作用域',
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 oauth2.oauth2_refresh_tokens 结构
CREATE TABLE IF NOT EXISTS `oauth2_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `appkey` varchar(40) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `expires` int(11) NOT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 oauth2.oauth2_tokens 结构
CREATE TABLE IF NOT EXISTS `oauth2_tokens` (
  `oauth_token` varchar(40) NOT NULL COMMENT 'access token',
  `app_key` varchar(128) NOT NULL COMMENT '应用标识',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `openid` varchar(128) NOT NULL COMMENT 'OPENID',
  `expires` int(11) NOT NULL COMMENT 'token过期时间',
  `scope` varchar(200) DEFAULT NULL COMMENT '作用域',
  `uninstall` int(10) NOT NULL DEFAULT '0' COMMENT '是否拆卸',
  PRIMARY KEY (`oauth_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
